package nzhi.apps.sidesa.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.activity.DashboardActivity;
import nzhi.apps.sidesa.activity.ListProdukActivity;
import nzhi.apps.sidesa.activity.ListSuratActivity;
import nzhi.apps.sidesa.activity.LoginActivity;

/**
 * Created by ASA on 12/14/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    NotificationChannel mChannel;
//    data_helper DH = new data_helper();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log

        Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);
        Log.e("JSON_OBJECT", object.toString());
        try {
            String title= object.getString("title");
            String type = object.getString("type");

            if(type.equals("1")){
                String message = "Maaf,Pengajuan Akun Ditolak";
                sendBroadcastNotification(title,message, type);
            }

            if(type.equals("1")){
                String message = "Selamat, Akun Anda telah Aktif";
                sendBroadcastNotification(title,message, type);
            }

            if(type.equals("2")){
                String message = "Surat Pengajuan Anda Telah Disetujui";
                sendBroadcastNotification(title,message, type);
            }

            if(type.equals("3")){
                String message = "Maaf, Surat Pengajuan Anda Tidak Disetujui";
                sendBroadcastNotification(title,message, type);
            }
            if(type.equals("4")){
                String message = "Produk Anda Diterima";
                sendBroadcastNotification(title,message, type);
            }
            if(type.equals("5")){
                String message = "Maaf, Produk Anda Ditolak";
                sendBroadcastNotification(title,message, type);
            }







            Log.d("Pesan title :", title);
            Log.d(TAG, "Pesan : " + remoteMessage.getData());


        } catch (JSONException e) {
            e.printStackTrace();
        }

//        1 : info
//        2 : promo , slider
//        3 : message ,
//        4 : order,
         // 5 : broadcast
    }

    //Notif Promo


    //Notif Broadcast
    private void sendBroadcastNotification(String Title, String shortMessage, String type) {
        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "Oemroh Jamaah";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }
        Intent intent = null;

        if(type.equals("1")){
           intent = new Intent(this, LoginActivity.class);


        }

        if(type.equals("2")){
           intent = new Intent(this, ListSuratActivity.class);


        }

        if(type.equals("3")){
           intent = new Intent(this, ListSuratActivity.class);


        }
        if(type.equals("4")){
           intent = new Intent(this, ListProdukActivity.class);


        }
        if(type.equals("5")){
           intent = new Intent(this, ListProdukActivity.class);

        }
        if(type.equals("0")){

        }






        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_logo_desa)
                .setContentTitle(Title)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setContentText(shortMessage)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(0, notificationBuilder.build());
    }
    //Notif Broadcast
    private void sendBroadcastNotification2(String Title, String shortMessage) {
        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "Oemroh Jamaah";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }



        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_logo_desa)
                .setContentTitle(Title)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setContentText(shortMessage)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(0, notificationBuilder.build());
    }
}
