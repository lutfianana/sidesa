package nzhi.apps.sidesa.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserHelper_sqlite extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Sidesa"; //DB name

    public UserHelper_sqlite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //MEMBUAT TABEL USER
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS user_member(nik VARCHAR, nama VARCHAR, nohp VARCHAR, password VARCHAR, loggedin_token TEXT, idoutlet VARCHAR, nama_outlet TEXT, role_id VARCHAR);");


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    //SIMPAN BT
    public void saveuser(String nik, String nama, String nohp, String password){
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "INSERT INTO user_member(nik, nama, nohp, password) VALUES " +
                "('" + nik + "','"+nama+"','"+nohp+"','"+password+"');";
        db.execSQL(query);

        db.close();
    }


    public String getobject(String strobject) {
        SQLiteDatabase db = this.getWritableDatabase();
        String res = null;
        String selectQuery = "SELECT " + strobject + " from user_member";
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                res = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        cursor.close();

        db.close();
        return res;

    }
    //UNTUK DELETE
    public void delete(String nama_tbl){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM "+nama_tbl;
        db.execSQL(query);
        db.close();
    }


}

