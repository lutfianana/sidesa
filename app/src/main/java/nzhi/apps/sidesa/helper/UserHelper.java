package nzhi.apps.sidesa.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserHelper {
    private Context mContext;
    private Activity mActivity;
    public static boolean showNumberJamaah, isLoginRegisfromHome, isServiceRunning = false, isMainAktif = true;
    public static int UId, FProfile; //jika 0 maka complete 0 (blm selesai)
    public static int isPaketStatus; //jika 1 pesan paket, jika 2 detail paket, 3 daftar jamaah
    public static String token_FCM;
    public static String lastLong = "1";
    public static String lastLat = "2";


    public static String status_popup = "1";
    public static String Jkode_grup,Jkelompok, Jmutowif, JNo_mutowif;
    public static String UNama, UEmail, UToken, UFoto;

    public static JSONArray JA_MemberJamaah;
    public static JSONObject JO_DetailPaket;
    public static JSONObject JO_ListTransaksi;
    public static JSONArray JA_MetodeBayar;

    public static List<String> List_Id_Jamaah = new ArrayList<String>();
    public static List<CharSequence> listMetodeBayar = new ArrayList<>();
    public static Bitmap bitmapImage;

    public UserHelper(Activity activity, Context context) {
        mContext = context;
        mActivity = activity;
    }
    public UserHelper() {

    }
}
