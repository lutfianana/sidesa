package nzhi.apps.sidesa.bridge;

public class AppConfig {
    public static String HOST = "http://apik.pekalongankab.go.id/android/";
    public static String URL_UPLOAD_REGIS= HOST+"register.php";
    public static String URL_Login = HOST+"mlogin.php";
    public static String URL_ListDataJenis = HOST+"mlistjenis.php";
    public static String URL_SendSurat = HOST+"msendsurat.php";
    public static String URL_ListDataTTD = HOST+"mlistdatattd.php";
    public static String URL_History = HOST+"mhistory.php";
    public static String URL_UPLOAD_PRODUK = HOST+"msendproduk.php";
    public static String URL_ListKategori = HOST+"mlistkategori.php";
    public static String URL_ListProduk = HOST+"mlistproduk.php";
    public static String URL_SendSuratFix = HOST+"msendsuratfix.php";
    public static String URL_HapusProduk = HOST+"mhapusproduk.php";
    public static String URL_EditInfoProduk = HOST+"meditinfoproduk.php";
    public static String URL_EditFotoProduk = HOST+"meditpotoproduk.php";
    public static String URL_EDIT_PRODUK1 = HOST+"meditpoto1.php";
    public static String URL_EDIT_PRODUK2 = HOST+"meditpoto2.php";
    public static String URL_EDIT_PRODUK3 = HOST+"meditpoto3.php";
    public static String URL_EDIT_PRODUK4 = HOST+"meditpoto4.php";

}
