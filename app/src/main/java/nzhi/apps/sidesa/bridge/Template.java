package nzhi.apps.sidesa.bridge;

public interface Template {

    interface ResponId{
        String Success = "00";  //    '00' : 'Success'
        String NoActive = "07";   //    '07' : 'Not Actived'
        String NoAuth = "09";   //    '09' : 'Not authorized'
        String NoData = "13";   //    '13' : 'No data'
        String Invalid = "19";  //    '19' : 'Invalid service'
        String Exist = "22";  //    '22' : 'Data already exist'
        String Unknown = "99";  //    '99' : 'Unknown error'
    }
    interface UrlPath{
        String Jamaah = "https://repo.oemroh.com/files/member/photo/";
        String Biro = "https://repo.oemroh.com/files/biro/logo/";
        String Maskapai = "https://repo.oemroh.com/files/general/maskapai/";
        String Hotel = "https://repo.oemroh.com/files/general/hotel/";
        String PaketUmroh = "https://repo.oemroh.com/files/biro/product/";
        String Slider = "https://repo.oemroh.com/files/portal/slider/";
        String Bank = AppConfig.HOST+"packages/upload/bank/";


    }
    interface ParamExtra{
        String IDPAKET = "idpaket";
        String KODEORDER = "kodeorder";
        String ISNOTIF = "notif";
        String CARI = "pencarian";
        String JENISCARI = "jeniscari";
        String KURSI = "jumlahkursi";
        String BIAYA = "kisaranbiaya";
        String BULANBERANGKAT = "bulanberangkat";
    }
    interface StatusOrder{
        int BelumBayar = 0;
        int MenungguVerifikasi = 1;
        int Lunas = 2;
    }
    interface Code{
        int CAMERA_IMAGE_CODE = 0;
        int CAMERA_VIDEO_CODE = 1;
        int FILE_MANAGER_CODE = 2;
        int ACCESS_FINE_LOCATION_INTENT_ID = 3;
        int GALLERY_KITKAT_INTENT_CALLED = 4;
        int AUDIO_CODE = 5;

    }
}
