package nzhi.apps.sidesa.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.adapter.adapter_list_surat;
import nzhi.apps.sidesa.bridge.AppConfig;
import nzhi.apps.sidesa.bridge.AppController;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;
import nzhi.apps.sidesa.model.item_data;

public class ListSuratActivity extends AppCompatActivity {
    Button btninputsurat;
    ProgressDialog pDialog;
    private List<item_data> ListMenu = new ArrayList<item_data>();
    private RecyclerView RVmenu;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }

    protected RecyclerView.LayoutManager mLayoutManager;
    protected ListSuratActivity.LayoutManagerType mCurrentLayoutManagerType;
    adapter_list_surat mAdapter;
    UserHelper_sqlite userHelper_sqlite;

    String tanggal, jammenit;

    private static String TAG = "AllDataOutletActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_surat);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Surat-menyurat");

        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        pDialog = new ProgressDialog(this);


        InitView();
        ReqListMenu();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void ReqListMenu(){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_History, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {


                    JSONArray data = new JSONArray(response);
                    if(data.length() > 0){
                        PasangData(data);

                    }
                    else{
                        DialogPesan("Warning", "Tidak Ada Riwayat Data");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("nik",userHelper_sqlite.getobject("nik"));



                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }



    private void InitView(){
        btninputsurat = findViewById(R.id.btninputsurat);
        btninputsurat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //surat
                Intent x = new Intent(getApplicationContext(), InputSuratActivity.class);
                startActivity(x);
            }
        });

        RVmenu = findViewById(R.id.RVHistory);
        RVmenu.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        RVmenu.setLayoutManager(mLayoutManager);

        mAdapter = new adapter_list_surat(getApplicationContext(),ListMenu);
        RVmenu.setAdapter(mAdapter);

    }

    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length();i++){
                JSONObject dataArray = data.getJSONObject(i);

                item_data item = new item_data();
                item.setIdSurat(dataArray.getString("idsrt_srt"));
                item.setIdJenis(dataArray.getString("idjsr_srt"));
                item.setStatusSurat(dataArray.getString("publish"));
                item.setTglSurat(dataArray.getString("tnggl_srt"));
                item.setKeperluanSurat(dataArray.getString("kprlu_srt"));
                item.setKetSurat(dataArray.getString("ktrgn_srt"));
             //   item.setNamaJenis(dataArray.getString());
               // item.setNamaLengkap(dataArray.getString(""));
                //   item.setTipe(dataArray.getString("nama_kategori"));
                //item.setImg_brand_url(Template.UrlPath.Biro+dataArray.getString("logo"));

                ListMenu.add(item);
            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            this.finish();


        }
        return super.onOptionsItemSelected(item);
    }


    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                    }
                })
                .show();
    }
}
