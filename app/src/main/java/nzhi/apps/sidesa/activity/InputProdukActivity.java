package nzhi.apps.sidesa.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.bridge.AppConfig;
import nzhi.apps.sidesa.bridge.AppController;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;
import nzhi.apps.sidesa.model.item_data;

import static android.R.layout.simple_spinner_item;

public class InputProdukActivity extends AppCompatActivity {
    Spinner spkategori;
    EditText ednamap, edeskripsi, edharga,edbrand, edtags;
    ImageView img1, img2, img3, img4;
    String strkategori;
    private String Document_img1="";
    private String Document_img2="";
    private String Document_img3="";
    private String Document_img4="";
    String picturePath, picturePath2, picturePath3, picturePath4;

    private String selectedFilePath;
    Button btnproses;
    ProgressDialog pDialog;
    private static String TAG = "InputProdukActivity";

    private ArrayList<item_data> ListData = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<String>();
    UserHelper_sqlite userHelper_sqlite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_produk);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Input Produk");

        userHelper_sqlite = new UserHelper_sqlite(getApplication());


        haveStoragePermission();
        /**request gambar**/


        /** inisialisasi**/
        spkategori = findViewById(R.id.spkatproduk);
        ednamap = findViewById(R.id.ednamap);
        edeskripsi = findViewById(R.id.edeskripsi);
        edharga = findViewById(R.id.edharga);
        edbrand = findViewById(R.id.edbrand);
        edtags = findViewById(R.id.edtag);
        img1 = findViewById(R.id.imgfoto1);
        img2 = findViewById(R.id.imgfoto2);
        img3 = findViewById(R.id.imgfoto3);
        img4 = findViewById(R.id.imgfoto4);
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                selectImage1(1);
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage1(2);
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage1(3);
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage1(4);
            }
        });

        btnproses = findViewById(R.id.btnkirim);
        pDialog = new ProgressDialog(this);
        ReqKategori();

        btnproses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
              //  SendProses(strkategori,);
                SendDetail();
            }
        });


    }


        @Override
        protected void onDestroy() {
            super.onDestroy();
            hidePDialog();
        }
        private void  showPDialog(String pesan){
            if (!pDialog.isShowing())
                pDialog.setMessage(pesan);
            pDialog.show();
        }
        private void hidePDialog() {
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
            //hapus


        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //jika diskip, maka
       finish();
       // askDelete();

    }


        private void ReqKategori(){
            StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                    AppConfig.URL_ListKategori, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, response.toString());
                    // hidePDialog();

                    try {
                        //    JSONObject jObj = new JSONObject(response);

                        JSONArray data = new JSONArray(response);
                        PasangSpinner(data);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    //hidePDialog();

                }
            }){

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<>();
                    params.put("start", "0");

                    return params;
                }

            };
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(Req_Absen);

        }


    private void PasangSpinner(JSONArray data){
        try{
            for(int i=0;i<data.length();i++){

                JSONObject jsonObject1=data.getJSONObject(i);

                item_data item = new item_data();
                item.setIdJenis(jsonObject1.getString("idcat_cat"));
                item.setNamaJenis(jsonObject1.getString("nama_cat"));

                ListData.add(item);
                //    CountryName.add(country);
            }

            for (int i = 0; i < ListData.size(); i++){
                names.add(ListData.get(i).getNamaJenis().toString());
            }


            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(InputProdukActivity.this, simple_spinner_item, names);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spkategori.setAdapter(spinnerArrayAdapter);
            spkategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String a = adapterView.getItemAtPosition(i).toString();
                    String id = ListData.get(i).getIdJenis();
                    Log.d("ID", id);
                    strkategori = id;


                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


        }catch (JSONException e){e.printStackTrace();}

    }
    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                        //finish();
                       Intent x = new Intent(getApplicationContext(), ListProdukActivity.class);
                        startActivity(x);
                        finish();
                    }
                })
                .show();
    }
    private void DialogPesanError(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                        //finish();

                    }
                })
                .show();
    }



    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }


    private void selectImage1(final int Code){
        final CharSequence[] options = { "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(InputProdukActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, Code);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.w("path of image from:", picturePath+"");
                img1.setImageBitmap(thumbnail);
                BitMapToString(thumbnail);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                picturePath2 = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath2));
                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.w("path of image from:", picturePath2+"");
                img2.setImageBitmap(thumbnail);
                BitMapToString2(thumbnail);
            }
            else if (requestCode == 3) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                picturePath3 = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath3));
                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.w("path of image2 from:", picturePath3+"");
                img3.setImageBitmap(thumbnail);
                BitMapToString3(thumbnail);
            }

            else if (requestCode == 4) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                picturePath4 = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath4));
                thumbnail=getResizedBitmap(thumbnail, 400);
                Log.w("path of image2 from:", picturePath4+"");
                img4.setImageBitmap(thumbnail);
                BitMapToString4(thumbnail);
            }


        }
    }


    public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userImage1.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        Document_img1 = Base64.encodeToString(b, Base64.DEFAULT);
        return Document_img1;
    }
    public String BitMapToString2(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userImage1.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        Document_img2 = Base64.encodeToString(b, Base64.DEFAULT);
        return Document_img2;
    }
    public String BitMapToString3(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userImage1.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        Document_img3 = Base64.encodeToString(b, Base64.DEFAULT);
        return Document_img3;
    }
    public String BitMapToString4(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userImage1.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        Document_img4 = Base64.encodeToString(b, Base64.DEFAULT);
        return Document_img4;
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void SendDetail(){

        final ProgressDialog loading = new ProgressDialog(this);
        loading.setMessage("Please Wait...");
        loading.show();
        loading.setCanceledOnTouchOutside(false);
        RetryPolicy mRetryPolicy = new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_UPLOAD_PRODUK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            loading.dismiss();
                            Log.d("JSON", response);
                            JSONObject jobj = new JSONObject(response);
                            String res = jobj.getString("response");
                            Log.d("JSON", res);

                            if(res.equals("00")){
                                DialogPesan("Sukses", "Upload Produk Berhasil");
                            }
                            else{
                                DialogPesanError("Error", "Maaf, Terjadi Kesalahan. Silakan Ulangi");

                            }
                        }catch(Exception e){
                            Log.d("Tag", e.getMessage());

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            ContextThemeWrapper ctw = new ContextThemeWrapper( InputProdukActivity.this, R.style.AppTheme);
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
                            alertDialogBuilder.setTitle("No connection");
                            alertDialogBuilder.setMessage(" Connection time out error please try again ");
                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            alertDialogBuilder.show();
                        } else if (error instanceof AuthFailureError) {
                            ContextThemeWrapper ctw = new ContextThemeWrapper( InputProdukActivity.this, R.style.AppTheme);
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
                            alertDialogBuilder.setTitle("Connection Error");
                            alertDialogBuilder.setMessage(" Authentication failure connection error please try again ");
                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            alertDialogBuilder.show();
                            //TODO
                        } else if (error instanceof ServerError) {
                            ContextThemeWrapper ctw = new ContextThemeWrapper( InputProdukActivity.this, R.style.AppTheme);
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
                            alertDialogBuilder.setTitle("Connection Error");
                            alertDialogBuilder.setMessage("Connection error please try again");
                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            alertDialogBuilder.show();
                            //TODO
                        } else if (error instanceof NetworkError) {
                            ContextThemeWrapper ctw = new ContextThemeWrapper( InputProdukActivity.this, R.style.AppTheme);
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
                            alertDialogBuilder.setTitle("Connection Error");
                            alertDialogBuilder.setMessage("Network connection error please try again");
                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            alertDialogBuilder.show();
                            //TODO
                        } else if (error instanceof ParseError) {
                            ContextThemeWrapper ctw = new ContextThemeWrapper( InputProdukActivity.this, R.style.AppTheme);
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctw);
                            alertDialogBuilder.setTitle("Error");
                            alertDialogBuilder.setMessage("Parse error");
                            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            alertDialogBuilder.show();
                        }
//                        Toast.makeText(Login_Activity.this,error.toString(), Toast.LENGTH_LONG ).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("image1_path",Document_img1);
                map.put("image2_path",Document_img2);
                map.put("image3_path",Document_img3);
                map.put("image4_path",Document_img4);
                map.put("image1_nama",userHelper_sqlite.getobject("nik")+edtags.getText().toString()+"1");
                map.put("image2_nama",userHelper_sqlite.getobject("nik")+edtags.getText().toString()+"2");
                map.put("image3_nama",userHelper_sqlite.getobject("nik")+edtags.getText().toString()+"3");
                map.put("image4_nama",userHelper_sqlite.getobject("nik")+edtags.getText().toString()+"4");
                map.put("strkategori", strkategori);
                map.put("strnama", ednamap.getText().toString());
                map.put("strnik", userHelper_sqlite.getobject("nik"));
                map.put("strlink",(ednamap.getText().toString()).replace(" ","-"));
                map.put("strdesc", edeskripsi.getText().toString());
                map.put("strharga", edharga.getText().toString());
                map.put("strbrand", edbrand.getText().toString());
                map.put("strtag", edtags.getText().toString());

                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(mRetryPolicy);
        requestQueue.add(stringRequest);
    }

    public  boolean haveStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("Permission error","You have permission");
                return true;
            } else {

                Log.e("Permission error","You have asked for permission");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //you dont need to worry about these stuff below api level 23
            Log.e("Permission error","You already have the permission");
            return true;
        }
    }

}
