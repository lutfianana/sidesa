package nzhi.apps.sidesa.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.bridge.AppConfig;
import nzhi.apps.sidesa.bridge.FileManager;
import nzhi.apps.sidesa.bridge.Template;
import nzhi.apps.sidesa.fcm.MyFirebaseInstanceIDService;
import nzhi.apps.sidesa.helper.SessionManager;
import nzhi.apps.sidesa.helper.UserHelper;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class RegisterActivity extends AppCompatActivity {
    Button btnregis, btnbrowsektp, btnbrowsefoto;
    EditText ednik,  ednama;
    TextView tvkelogin;
    ImageView imgfoto, imgktp;
    //untuk upload gbr
    Intent intent;
    private static String TAG ="RegisterActivity";
    ProgressDialog dialog;
    public  static final int RequestPermissionCode  = 1 ;
    private static final int STORAGE_PERMISSION_CODE = 123;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    Bitmap bitmap;
    boolean check = true;
    ProgressDialog pDialog;

    String strnik;
    String ImageNameFieldOnServer = "image_name" ;
    String ImagePathFieldOnServer = "image_path" ;
    UserHelper_sqlite userHelper_sqlite;
    private FirebaseAuth mAuth; //untuk auth
    SessionManager session;


    private static String[] CHOOSE_FILE = {"Camera", "File manager"};
    private Uri mOutputUri = null;
    private File compressedImageFile;
    OkHttpClient client;
    RequestBody requestBody;
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        dialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        session = new SessionManager(getApplicationContext());
        SaveToken();
        CheckingSession();
        EnableRuntimePermissionToAccessCamera();

        pDialog= new ProgressDialog(this);
        btnregis = findViewById(R.id.btn_regis);
        imgfoto = findViewById(R.id.imgfoto);
        imgktp = findViewById(R.id.imgktp);
        ednik = findViewById(R.id.ednik);
        ednama = findViewById(R.id.ednama);

        strnik = ednik.getText().toString();

        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        imgfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //send ke server
               // ImageUploadToServerFunction();
                /*intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 7);*/
                dialogfoto();

            }
        });



        btnregis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //

            }
        });

        tvkelogin = findViewById(R.id.tvlinklogin);
        tvkelogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent kelogin = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(kelogin);
            }
        });


    }
    private void showPDialog(String pesan) {
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }

    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



    // Requesting runtime permission to access camera.
    public void EnableRuntimePermissionToAccessCamera(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(RegisterActivity.this,
                Manifest.permission.CAMERA))
        {
            // Printing toast message after enabling runtime permission.
            Toast.makeText(RegisterActivity.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(RegisterActivity.this,new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);

        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      /*  if (requestCode == 7 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            try {
                // Adding captured image in bitmap.
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // adding captured image in imageview.
                imgfoto.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();
            }
        }*/
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Template.Code.FILE_MANAGER_CODE) {
                //onSelectFromGalleryResult(data);
                mOutputUri = data.getData();
                //setFile(requestCode, mOutputUri);
                //setView(requestCode, mOutputUri);
                CompressImage(getFile(requestCode, mOutputUri));
            } else if (requestCode == Template.Code.CAMERA_IMAGE_CODE) {
                //onCaptureImageResult(data);
                //setFile(requestCode, mOutputUri);
                //setView(requestCode, mOutputUri);
                CompressImage(getFile(requestCode, mOutputUri));
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(RegisterActivity.this,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(RegisterActivity.this,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }



    private void dialogfoto(){
        new MaterialDialog.Builder(this)
                .title("Pilihan")
                .items(CHOOSE_FILE)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        requestStoragePermission();
                        if (position == 0) {
                            if (hasCameraPermission()) {
                                takeFromCamera();
                            } else {
                                openCameraPermission();
                            }
                        } else {
                            galleryIntent();
                        }
                    }
                })
                .show();
    }

    private void SaveToken(){
        if (session.isTokenIn()!=null){
            UserHelper.token_FCM = session.isTokenIn();
            Log.d("Token Tersimpan", UserHelper.token_FCM);
            //Toast.makeText(this, "Token Siap", Toast.LENGTH_SHORT).show();
        }else if (UserHelper.token_FCM !=null){
            session.setToken(UserHelper.token_FCM);
            Log.d("Token di simpan ke pref", UserHelper.token_FCM);
            //Toast.makeText(this, "Token Siap", Toast.LENGTH_SHORT).show();
        }else {
            //AmbilData();
            Log.d("Token null", "");

            MyFirebaseInstanceIDService mFS = new MyFirebaseInstanceIDService();
            mFS.getTokenID();
            // Log.d("get New Token", UserHelper.token_FCM);

            //Toast.makeText(this, "Token Else : "+UserHelper.token_FCM, Toast.LENGTH_SHORT).show();
            SaveToken();
        }
    }


    private void CheckingSession(){
        if (session.isLoggedIn() && userHelper_sqlite.getobject("nik") !=null ) {
            // Fetching user details from sqlite
           /* Users user = RC.getFirstUser();
            UserHelper.UId = user.getId();
            UserHelper.UEmail = user.getEmail();
            UserHelper.UNama = user.getNama();
            // User is already logged in. Take him to main activity, langsung ke
*/
            Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(i);
            finish();
        }
    }




    /**kebutuhan upload**/

    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    private boolean hasCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void openCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    Template.Code.CAMERA_IMAGE_CODE);
        }
    }


    private void takeFromCamera() {
        if(checkPermissionREAD_EXTERNAL_STORAGE(getApplicationContext())){
            //Mengambil foto dengan camera
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            //for run > android 6(Mars)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                mOutputUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new ContentValues());
            } else {
                mOutputUri = FileManager.getOutputMediaFileUri(Template.Code.CAMERA_IMAGE_CODE);
            }

            if (hasImageCaptureBug()) {
                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File("/sdcard/tmp")));
                Log.d(TAG, "takeFromCamera: Bug On");
            } else {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputUri);
                Log.d(TAG, "takeFromCamera: Bug Off");
                //intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            }

            Log.d(TAG, "takeFromCamera: " + mOutputUri);
            startActivityForResult(intent, Template.Code.CAMERA_IMAGE_CODE);
        }



    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), Template.Code.FILE_MANAGER_CODE);
    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", getApplicationContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    public boolean hasImageCaptureBug() {

        // list of known devices that have the bug
        ArrayList<String> devices = new ArrayList<>();
        devices.add("android-devphone1/dream_devphone/dream");
        devices.add("generic/sdk/generic");
        devices.add("vodafone/vfpioneer/sapphire");
        devices.add("tmobile/kila/dream");
        devices.add("verizon/voles/sholes");
        devices.add("google_ion/google_ion/sapphire");

        return devices.contains(android.os.Build.BRAND + "/" + android.os.Build.PRODUCT + "/"
                + android.os.Build.DEVICE);

    }


    private void CompressImage(File srcImg) {
        try {
            Log.d(TAG, "Path Before Compress: " + srcImg.getAbsolutePath());
            Log.d(TAG, "Size Before Compress: " + getFileSize(srcImg));

            //Toast.makeText(getActivity(), "Sebelum : "+getFileSize(srcImg), Toast.LENGTH_SHORT).show();

            //compress image hasil file
            compressedImageFile = new Compressor(getApplicationContext()).compressToFile(srcImg);

            //tampilkan image
            //TampilImageAfterCompress(compressedImageFile);

            Log.d(TAG, "Path After Compress: " + compressedImageFile.getAbsolutePath());
            Log.d(TAG, "Size After Compress: " + getFileSize(compressedImageFile));
            //Toast.makeText(getActivity(), "Stelah : "+getFileSize(compressedImageFile), Toast.LENGTH_SHORT).show();

            AskForUpload();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void AskForUpload() {
        new MaterialDialog.Builder(this)
                .title("Upload Foto")
                .content("apakah anda yakin akan mengupload foto tersebut?")
                .positiveText("Kirim")
                .neutralText("Batal")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //Toast.makeText(getActivity(), "Uploading", Toast.LENGTH_SHORT).show();
                        showPDialog("Uploading ...");
                        getRequestUpload();
                        UploadFile();
                    }
                })
                .show();
    }
    private void UploadFile() {

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(AppConfig.URL_UPLOAD_REGIS)
                .post(requestBody)
                .build();


        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    hidePDialog();
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull okhttp3.Response response) throws IOException {
                    final String textResponse = response.body().string();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "run: " + textResponse);
                            try {
                                final JSONObject jObj = new JSONObject(textResponse);
                                String KodeRespon = jObj.getString("response");
                                if (KodeRespon.equals(Template.ResponId.Success)) {
                                    //apdet foto
                                   // userHelper_sqlite.update_foto(getFilename(compressedImageFile), userHelper_sqlite.getobject("email"));

                                    TampilImageAfterUpload(compressedImageFile);
                                } else {
                                   // Log.e(TAG, "Failed Upload Foto : " + helper.getNameCode(jObj.getString("response")));
                               //     Toast.makeText(getApplicationContext(), jObj.getString("response") + " : " + helper.getNameCode(jObj.getString("response")), Toast.LENGTH_SHORT).show();
                                }
                                hidePDialog();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getRequestUpload() {
        requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                //.addFormDataPart("submit", "true")
                .addFormDataPart("email", userHelper_sqlite.getobject("email"))
                .addFormDataPart("path", "photo")
                .addFormDataPart("filename", getFilename(compressedImageFile))
                .addFormDataPart("file", compressedImageFile.getAbsolutePath(),
                        RequestBody.create(MEDIA_TYPE_PNG, compressedImageFile))
                .build();

        Log.d("path", compressedImageFile.getAbsolutePath());
        Log.d("filename", getFilename(compressedImageFile));
        Log.d("FILE", compressedImageFile.getAbsolutePath());
    }

    private void TampilImageAfterUpload(File file) {
        Glide
                .with(this)
                .load(file)
                .asBitmap()
                .placeholder(R.drawable.ic_launcher_background)
                .into(imgfoto);
        imgfoto.setVisibility(ImageView.VISIBLE);
    }
    private String getFilename(File file) {
        return file.getName();
    }

    private File getFile(int type, Uri uri) {
        //File mediaStorageDir = new File(getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), TAG);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            return new File(getImagePath(uri));
        } else {
            return new File(FileManager.getPath(getApplicationContext(), type, uri));
        }
    }
    private String getFileSize(File oldFile) {
        File file = new File(oldFile.getAbsolutePath());
        int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
        return String.valueOf(file_size);
    }

    public String getImagePath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }



    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[] { permission },
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

}
