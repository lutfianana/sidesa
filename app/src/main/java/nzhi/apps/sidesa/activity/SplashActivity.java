package nzhi.apps.sidesa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;

public class SplashActivity extends AppCompatActivity {
    //  Button btnkelogin;
    private final int SPLASH_DISPLAY_LENGTH = 3;
    UserHelper_sqlite userHelper_sqlite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        //   getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Delay();

    }
    private void Delay(){
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                //cek dulu

                if(userHelper_sqlite.getobject("nik") == null ){
                    //jika tdk login
                    Intent kelog = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(kelog);
                    finish();
                }
                else {
                    //ke dashboard
                    Intent kelog = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(kelog);
                    finish();


                }
                //ke dashboard




          /*      Intent kelog = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(kelog);
                finish();*/



            }
        }, (SPLASH_DISPLAY_LENGTH*1000));
    }

}
