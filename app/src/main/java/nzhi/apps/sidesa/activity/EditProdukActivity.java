package nzhi.apps.sidesa.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.bridge.AppConfig;
import nzhi.apps.sidesa.bridge.AppController;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;
import nzhi.apps.sidesa.model.item_data;

import static android.R.layout.simple_spinner_item;

public class EditProdukActivity extends AppCompatActivity {
    String stridproduk, strkategori, strnamaproduk, strdescproduk, strhargaprod, strbrandprod, strtags;
    Spinner spkategori;
    EditText ednamaproduk, edeskripsi, edharga, edbrand, edtags;
    Button btnsend;
    private static String TAG = "EditProdukActivity";
    private ArrayList<item_data> ListData = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<String>();
    ProgressDialog pDialog;
    UserHelper_sqlite userHelper_sqlite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_produk);

        pDialog = new ProgressDialog(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Edit Info Produk");

        userHelper_sqlite = new UserHelper_sqlite(getApplication());

        stridproduk = getIntent().getStringExtra("idproduk");
        strnamaproduk = getIntent().getStringExtra("nama_prd");
        strdescproduk = getIntent().getStringExtra("desc_prd");
        strtags = getIntent().getStringExtra("tag_prd");
        strhargaprod = getIntent().getStringExtra("harga_prd");
        strbrandprod = getIntent().getStringExtra("brand_Prd");


        spkategori = findViewById(R.id.spkatproduk);
        ednamaproduk = findViewById(R.id.ednamap);
        edeskripsi = findViewById(R.id.edeskripsi);
        edharga = findViewById(R.id.edharga);
        edbrand = findViewById(R.id.edbrand);
        edtags = findViewById(R.id.edtag);
        btnsend = findViewById(R.id.btnkirim);



        ednamaproduk.setText(strnamaproduk);
        edeskripsi.setText(strdescproduk);
        edharga.setText(strhargaprod);
        edbrand.setText(strbrandprod);
        edtags.setText(strtags);

        //
        ReqKategori();
        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendEditInfo(stridproduk, strkategori,ednamaproduk.getText().toString(), edeskripsi.getText().toString(),edharga.getText().toString(),edbrand.getText().toString(),edtags.getText().toString());
            }
        });



    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
            //hapus


        }
        return super.onOptionsItemSelected(item);
    }
    private void ReqKategori(){
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_ListKategori, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                // hidePDialog();

                try {
                    //    JSONObject jObj = new JSONObject(response);

                    JSONArray data = new JSONArray(response);
                    PasangSpinner(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("start", "0");

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);

    }


    private void PasangSpinner(JSONArray data){
        try{
            for(int i=0;i<data.length();i++){

                JSONObject jsonObject1=data.getJSONObject(i);

                item_data item = new item_data();
                item.setIdJenis(jsonObject1.getString("idcat_cat"));
                item.setNamaJenis(jsonObject1.getString("nama_cat"));

                ListData.add(item);
                //    CountryName.add(country);
            }

            for (int i = 0; i < ListData.size(); i++){
                names.add(ListData.get(i).getNamaJenis().toString());
            }


            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(EditProdukActivity.this, simple_spinner_item, names);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spkategori.setAdapter(spinnerArrayAdapter);
            spkategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String a = adapterView.getItemAtPosition(i).toString();
                    String id = ListData.get(i).getIdJenis();
                    Log.d("ID", id);
                    strkategori = id;


                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


        }catch (JSONException e){e.printStackTrace();}

    }

    //SendEditInfo(stridproduk, strkategori,ednamaproduk.getText().toString(), edeskripsi.getText().toString(),edharga.getText().toString(),edbrand.getText().toString(),edtags.getText().toString());

    private void SendEditInfo(final String stridproduk, final String strkategori, final String strnamaproduk, final String strdescproduk, final String strhargaproduk, final String strbrandprod, final String strtag){
        showPDialog("Loading ..");
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_EditInfoProduk, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){
                        DialogPesanSukses("Sukses", "Info Produk Berhasil Diubah");
                    }
                    else {
                        DialogPesan("Gagal", "Terjadi Kesalahan, Silakan ulangi");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("stridproduk", stridproduk);
                params.put("strkategori", strkategori);
                params.put("strnama", strnamaproduk);
                params.put("strnik", userHelper_sqlite.getobject("nik"));
                params.put("strlink",strnamaproduk.replace(" ","-"));
                params.put("strdesc", strdescproduk);
                params.put("strharga", strhargaproduk);
                params.put("strbrand", strbrandprod);
                params.put("strtag", strtag);
                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);

    }

    private void SetJudul(final String strjudul){
        getSupportActionBar().setTitle(strjudul);
    }
    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                        //finish();

                    }
                })
                .show();
    }
    private void DialogPesanSukses(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                        //finish();
                        Intent x = new Intent(getApplicationContext(), ListProdukActivity.class);
                        startActivity(x);
                        finish();

                    }
                })
                .show();
    }


}
