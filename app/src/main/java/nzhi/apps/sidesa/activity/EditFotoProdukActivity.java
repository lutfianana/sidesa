package nzhi.apps.sidesa.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;

import nzhi.apps.sidesa.R;

public class EditFotoProdukActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    ImageView img1, img2, img3, img4;
    String url1, url2, url3, url4;
    String idproduk, tagproduk;
    TextView tvedit1, tvedit2, tvedit3, tvedit4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editfoto_produk);

        pDialog = new ProgressDialog(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Edit Foto Produk");
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);

        tvedit1 = findViewById(R.id.tvedit1);
        tvedit2 = findViewById(R.id.tvedit2);
        tvedit3 = findViewById(R.id.tvedit3);
        tvedit4 = findViewById(R.id.tvedit4);

        url1 = getIntent().getStringExtra("url1");
        url2 = getIntent().getStringExtra("url2");
        url3 = getIntent().getStringExtra("url3");
        url4 = getIntent().getStringExtra("url4");
        idproduk = getIntent().getStringExtra("idproduk");
        tagproduk = getIntent().getStringExtra("tag_prd");

        tvedit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                Intent x = new Intent(getApplicationContext(), EditFoto1Activity.class);
                x.putExtra("idproduk", idproduk);
                x.putExtra("tagproduk",tagproduk);
                startActivity(x);
                
            }
        });

        tvedit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(getApplicationContext(), EditFoto2Activity.class);
                x.putExtra("idproduk", idproduk);
                x.putExtra("tagproduk",tagproduk);
                startActivity(x);
            }
        });

        tvedit3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(getApplicationContext(), EditFoto1Activity.class);
                x.putExtra("idproduk", idproduk);
                x.putExtra("tagproduk",tagproduk);
                startActivity(x);
            }
        });
        tvedit4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(getApplicationContext(), EditFoto1Activity.class);
                x.putExtra("idproduk", idproduk);
                x.putExtra("tagproduk",tagproduk);
                startActivity(x);
            }
        });



        //set 1
        Glide.with(getApplicationContext())
                .load(url1)
                .error(R.drawable.kaos)
                .fitCenter()
                .into(img1);
        Glide.with(getApplicationContext())
                .load(url2)
                .error(R.drawable.kaos)
                .fitCenter()
                .into(img2);

        Glide.with(getApplicationContext())
                .load(url3)
                .error(R.drawable.kaos)
                .fitCenter()
                .into(img3);

        Glide.with(getApplicationContext())
                .load(url4)
                .error(R.drawable.kaos)
                .fitCenter()
                .into(img4);


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            this.finish();


        }
        return super.onOptionsItemSelected(item);
    }

    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }

    }
