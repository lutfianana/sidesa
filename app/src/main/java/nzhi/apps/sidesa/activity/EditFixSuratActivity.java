package nzhi.apps.sidesa.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.bridge.AppConfig;
import nzhi.apps.sidesa.bridge.AppController;
import nzhi.apps.sidesa.helper.UserHelper;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;
import nzhi.apps.sidesa.model.item_data;

import static android.R.layout.simple_spinner_item;

public class EditFixSuratActivity extends AppCompatActivity {
    Spinner spjenis;
    TextView tvtanggal;
    EditText ednama, edkeperluan, edketerangan;
    Button btnkirim;
    ProgressDialog pDialog;
    String stridsurat,stridjenis, strnama, strtanggal, strketerangan, strkeperluan;
    String strjenis;
    private static String TAG = "EditFixSuratActivity";
    private ArrayList<item_data> ListData = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<String>();
    DatePickerDialog datePickerDialog;
    SimpleDateFormat df;
    Date c;
    String formattedDate;
    UserHelper_sqlite userHelper_sqlite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_surat);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Konfirmasi Surat");

        InitView();
        ReqDataJenis();

    }

    private void InitView(){
        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);

        pDialog = new ProgressDialog(this);
        userHelper_sqlite = new UserHelper_sqlite(getApplication());

        spjenis = findViewById(R.id.tvpilihjenis);
        tvtanggal = findViewById(R.id.edtanggal);
        ednama = findViewById(R.id.ednama);
        edkeperluan = findViewById(R.id.edkeperluan);
        edketerangan = findViewById(R.id.edketerangan);
        btnkirim = findViewById(R.id.btnkirim);

        stridsurat = getIntent().getStringExtra("stridsurat");
        stridjenis = getIntent().getStringExtra("strjenis");
        strnama = getIntent().getStringExtra("strnama");
        strtanggal = getIntent().getStringExtra("strtanggal");
        strketerangan = getIntent().getStringExtra("strketerangan");
        strkeperluan = getIntent().getStringExtra("strkeperluan");

        //set

        tvtanggal.setText(strtanggal);
        ednama.setText(strnama);
        edkeperluan.setText(strkeperluan);
        edketerangan.setText(strketerangan);

        tvtanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                DatePicker("tanggal");
            }
        });

        btnkirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //set
                KonfirmDulu(strjenis, tvtanggal.getText().toString(), ednama.getText().toString(), edkeperluan.getText().toString(), edketerangan.getText().toString());

            }
        });





    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void ReqDataJenis(){
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_ListDataJenis, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                // hidePDialog();

                try {
                    //    JSONObject jObj = new JSONObject(response);

                    JSONArray data = new JSONArray(response);
                    PasangSpinner(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("start", "0");

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);

    }

    private void PasangSpinner(JSONArray data){
        try{
            for(int i=0;i<data.length();i++){

                JSONObject jsonObject1=data.getJSONObject(i);

                item_data item = new item_data();
                item.setIdJenis(jsonObject1.getString("idjsr_jsr"));
                item.setNamaJenis(jsonObject1.getString("nama_jsr"));

                ListData.add(item);
                //    CountryName.add(country);
            }

            for (int i = 0; i < ListData.size(); i++){
                names.add(ListData.get(i).getNamaJenis().toString());
            }

            String compareValue = "1";
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(EditFixSuratActivity.this, simple_spinner_item, names);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spjenis.setAdapter(spinnerArrayAdapter);
            if (compareValue != null) {
                int spinnerPosition = spinnerArrayAdapter.getPosition(compareValue);
                spjenis.setSelection(spinnerPosition);
            }


            spjenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    String a = adapterView.getItemAtPosition(i).toString();
                    String id = ListData.get(i).getIdJenis();
                    Log.d("ID", id);
                    strjenis = id;


                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


        }catch (JSONException e){e.printStackTrace();}

    }

    private void DatePicker(final String ed){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                if(ed.equals("tanggal")){
                    tvtanggal.setText(df.format(newDate.getTime()));
                }

            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();


    }

    private void KonfirmDulu(final String strjenis, final String strtanggal, final String strnama, final String strkeperluan, final String strketerangan){
        new MaterialDialog.Builder(this)
                .title("Konfirmasi")
                .content("Yakin Kirim Sekarang? ")
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                       SendData(stridsurat,strjenis, strtanggal,strnama,strkeperluan,strketerangan);
                    }
                })
                .show();
    }
    private void SendData(final String stridsurat, final String strjenis, final String strtanggal, final String strnama, final String strkeperluan, final String strketerangan){
        showPDialog("Loading ..");
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_SendSuratFix, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){
                        DialogPesanSukses("Sukses", "Data Permohonan Berhasil Diubah");
                    }
                    else {
                        DialogPesan("Gagal", "Terjadi Kesalahan, Silakan ulangi");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("idsurat",stridsurat);
                params.put("idjenis",strjenis);
                params.put("strtanggal", strtanggal);
                params.put("strnik", userHelper_sqlite.getobject("nik"));
                params.put("strnama", strnama);
                params.put("strkeperluan", strkeperluan);
                params.put("strketerangan", strketerangan);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);

    }

    private void DialogPesanSukses(final String judul, final String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                        Intent list = new Intent(getApplicationContext(), ListSuratActivity.class);
                        startActivity(list);
                        finish();
                    }
                })
                .show();
    }
    private void DialogPesan(final String judul, final String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                    }
                })
                .show();
    }

    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            this.finish();


        }
        return super.onOptionsItemSelected(item);
    }

}
