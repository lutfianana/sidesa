package nzhi.apps.sidesa.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.bridge.AppConfig;
import nzhi.apps.sidesa.bridge.AppController;
import nzhi.apps.sidesa.fcm.MyFirebaseInstanceIDService;
import nzhi.apps.sidesa.helper.SessionManager;
import nzhi.apps.sidesa.helper.UserHelper;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;

public class LoginActivity extends AppCompatActivity {
    TextView tvlinkeregis;
    EditText ednik, edpassword;
    Button btnlogin;
    ProgressDialog pDialog;
    private static String TAG ="LoginActivity";
    UserHelper_sqlite userHelper_sqlite;
    private FirebaseAuth mAuth; //untuk auth
    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();


        tvlinkeregis = findViewById(R.id.tvlinkdaftar);
        session = new SessionManager(getApplicationContext());
        mAuth = FirebaseAuth.getInstance();
        SaveToken();
        CheckingSession();


        tvlinkeregis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                Intent keregis = new Intent(getApplicationContext() , RegisterActivity2.class);
                startActivity(keregis);
            }
        });

        pDialog = new ProgressDialog(this);
        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        ednik = findViewById(R.id.ednik);
        edpassword  = findViewById(R.id.edpassword);
        btnlogin = findViewById(R.id.btn_login);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Req_log(ednik.getText().toString(), edpassword.getText().toString());
            }
        });




    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void Req_log(final String nik, final String password){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_register = new StringRequest(Request.Method.POST,
                AppConfig.URL_Login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject( response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){
                        JSONObject data = jObj.getJSONObject("data");
                        // String ni = data.getString("nim");
                        String nama = data.getString("nama_pk");
                        String nik = data.getString("nik_pd");

                        //save
                        userHelper_sqlite.saveuser(nik,nama,"",password);
                        Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
                        startActivity(dash);
                        finish();
                    }
                    else if(KodeRespon.equals("05")){
                        DialogPesan("Login Gagal", "Akun Anda Belum Aktif. Silakan Hubungi Admin");
                    }
                    else {
                        DialogPesan("Login Gagal", "Silakan ulangi");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nik", nik);
                params.put("password", password);
                params.put("fcm_token", UserHelper.token_FCM);
                //params.put("role_type", "1");


                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_register);
    }

    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                    }
                })
                .show();
    }

    private void SaveToken(){
        if (session.isTokenIn()!=null){
            UserHelper.token_FCM = session.isTokenIn();
            Log.d("Token Tersimpan", UserHelper.token_FCM);
            //Toast.makeText(this, "Token Siap", Toast.LENGTH_SHORT).show();
        }else if (UserHelper.token_FCM !=null){
            session.setToken(UserHelper.token_FCM);
            Log.d("Token di simpan ke pref", UserHelper.token_FCM);
            //Toast.makeText(this, "Token Siap", Toast.LENGTH_SHORT).show();
        }else {
            //AmbilData();
            Log.d("Token null", "");

            MyFirebaseInstanceIDService mFS = new MyFirebaseInstanceIDService();
            mFS.getTokenID();
            // Log.d("get New Token", UserHelper.token_FCM);

            //Toast.makeText(this, "Token Else : "+UserHelper.token_FCM, Toast.LENGTH_SHORT).show();
            SaveToken();
        }
    }


    private void CheckingSession(){
        if (session.isLoggedIn() && userHelper_sqlite.getobject("nik") !=null ) {
            // Fetching user details from sqlite
           /* Users user = RC.getFirstUser();
            UserHelper.UId = user.getId();
            UserHelper.UEmail = user.getEmail();
            UserHelper.UNama = user.getNama();
            // User is already logged in. Take him to main activity, langsung ke
*/
            Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(i);
            finish();
        }
    }





}
