package nzhi.apps.sidesa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;

public class DashboardActivity extends AppCompatActivity {
    CardView cvinputsurat, cvinputproduk, cveditprofil, cvriwayat;
    TextView tvnama, tvnik;
    Button btnlogout;
    UserHelper_sqlite userHelper_sqlite;
    Intent x;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_all);
        getSupportActionBar().hide();


        userHelper_sqlite = new UserHelper_sqlite(getApplication());

        btnlogout = findViewById(R.id.btnlogout);
        cvinputproduk = findViewById(R.id.CVInputproduk);
        cvinputsurat = findViewById(R.id.CVInputsurat);
        cveditprofil = findViewById(R.id.CVnotif);
        cvriwayat = findViewById(R.id.CVRiwayat);

        tvnama = findViewById(R.id.tvnama);
        tvnik = findViewById(R.id.tvnik);

        tvnama.setText(userHelper_sqlite.getobject("nama"));
        tvnik.setText(userHelper_sqlite.getobject("nik"));

        cvinputsurat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                x = new Intent(getApplicationContext(), ListSuratActivity.class);
                startActivity(x);

            }
        });

        cvinputproduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                x = new Intent(getApplicationContext(), ListProdukActivity.class);
                startActivity(x);
            }
        });

        cvriwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                x = new Intent(getApplicationContext(), ListSuratActivity.class);
                startActivity(x);
            }
        });

        cveditprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                x = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(x);
            }
        });
        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askDelete();
            }
        });


    }


    private void askDelete(){
        new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content("Anda Yakin Ingin Keluar? ")
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        userHelper_sqlite.delete("user_member");
                        Intent home = new Intent(getApplicationContext(), LoginActivity.class);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(home);
                        finish();
                    }
                })
                .show();
        }
    }
