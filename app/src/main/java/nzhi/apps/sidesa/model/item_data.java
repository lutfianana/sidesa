package nzhi.apps.sidesa.model;

public class item_data {
    private String idJenis;
    private String NamaJenis;
    private String NamaSurat;
    private String NamaLengkap;
    private String TglSurat;
    private String StatusSurat;
    private String KeperluanSurat;
    private String KetSurat;
    private String IdSurat;

    private String IdProduk;
    private String NamaProduk;
    private String UrlProduk;
    private String DescProduk;
    private String HargaProduk;
    private String BrandProduk;
    private String TagProduk;
    private String URLProduk1;
    private String URLProduk2;
    private String URLProduk3;
    private String URLProduk4;


    public String getIdSurat() {
        return IdSurat;
    }

    public String getNamaLengkap() {
        return NamaLengkap;
    }

    public String getIdJenis() {
        return idJenis;
    }

    public String getNamaJenis() {
        return NamaJenis;
    }

    public String getNamaSurat() {
        return NamaSurat;
    }

    public String getStatusSurat() {
        return StatusSurat;
    }

    public String getKeperluanSurat() {
        return KeperluanSurat;
    }

    public String getKetSurat() {
        return KetSurat;
    }

    public String getTglSurat() {
        return TglSurat;
    }

    public String getIdProduk() {
        return IdProduk;
    }

    public String getDescProduk() {
        return DescProduk;
    }

    public String getNamaProduk() {
        return NamaProduk;
    }

    public String getUrlProduk() {
        return UrlProduk;
    }

    public String getHargaProduk() {
        return HargaProduk;
    }

    public String getBrandProduk() {
        return BrandProduk;
    }

    public String getTagProduk() {
        return TagProduk;
    }

    public String getURLProduk1() {
        return URLProduk1;
    }

    public String getURLProduk2() {
        return URLProduk2;
    }

    public String getURLProduk3() {
        return URLProduk3;
    }

    public String getURLProduk4() {
        return URLProduk4;
    }

    public void setIdSurat(String idSurat) {
        IdSurat = idSurat;
    }

    public void setNamaLengkap(String namaLengkap) {
        NamaLengkap = namaLengkap;
    }

    public void setIdJenis(String idJenis) {
        this.idJenis = idJenis;
    }

    public void setNamaJenis(String namaJenis) {
        NamaJenis = namaJenis;
    }

    public void setNamaSurat(String namaSurat) {
        NamaSurat = namaSurat;
    }

    public void setStatusSurat(String statusSurat) {
        StatusSurat = statusSurat;
    }

    public void setTglSurat(String tglSurat) {
        TglSurat = tglSurat;
    }

    public void setKeperluanSurat(String keperluanSurat) {
        KeperluanSurat = keperluanSurat;
    }

    public void setKetSurat(String ketSurat) {
        KetSurat = ketSurat;
    }

    public void setDescProduk(String descProduk) {
        DescProduk = descProduk;
    }

    public void setNamaProduk(String namaProduk) {
        NamaProduk = namaProduk;
    }

    public void setIdProduk(String idProduk) {
        IdProduk = idProduk;
    }

    public void setUrlProduk(String urlProduk) {
        UrlProduk = urlProduk;
    }

    public void setHargaProduk(String hargaProduk) {
        HargaProduk = hargaProduk;
    }

    public void setBrandProduk(String brandProduk) {
        BrandProduk = brandProduk;
    }

    public void setTagProduk(String tagProduk) {
        TagProduk = tagProduk;
    }

    public void setURLProduk1(String URLProduk1) {
        this.URLProduk1 = URLProduk1;
    }

    public void setURLProduk2(String URLProduk2) {
        this.URLProduk2 = URLProduk2;
    }

    public void setURLProduk3(String URLProduk3) {
        this.URLProduk3 = URLProduk3;
    }

    public void setURLProduk4(String URLProduk4) {
        this.URLProduk4 = URLProduk4;
    }
}
