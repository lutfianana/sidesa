package nzhi.apps.sidesa.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.model.item_data;

public class adapter_jenis_surat extends RecyclerView.Adapter<adapter_jenis_surat.MyViewHolder> {
    private List<item_data> itemhomeList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        //konten2 yang ada di dlm adapter
        TextView tvjenis;

        public MyViewHolder(View view){
            super(view);

            tvjenis = (TextView) view.findViewById(R.id.tvjenis);

        }

    }

    public adapter_jenis_surat(Context context, List<item_data> itemhomeList) {
        mContext = context;
        this.itemhomeList= itemhomeList;
    }

    @NonNull
    @Override
    public adapter_jenis_surat.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_jenis, parent, false);

        return new adapter_jenis_surat.MyViewHolder(itemView);
        // return null;
    }

    @Override
    public void onBindViewHolder(@NonNull adapter_jenis_surat.MyViewHolder holder, int position) {
        final item_data item = itemhomeList.get(position);

        holder.tvjenis.setText(item.getNamaJenis());



    }

    @Override
    public int getItemCount() {
        return itemhomeList.size();
    }
}

