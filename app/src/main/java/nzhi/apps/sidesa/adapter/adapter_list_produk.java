package nzhi.apps.sidesa.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.activity.EditFotoProdukActivity;
import nzhi.apps.sidesa.activity.EditProdukActivity;
import nzhi.apps.sidesa.bridge.AppConfig;
import nzhi.apps.sidesa.bridge.AppController;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;
import nzhi.apps.sidesa.model.item_data;

public class adapter_list_produk extends RecyclerView.Adapter<adapter_list_produk.MyViewHolder> {
    private List<item_data> ListMenu;
    private Context mContext;
    UserHelper_sqlite userHelper_sqlite;
    ProgressDialog pDialog;
    private String pilihan;
    String idproduk;
    item_data item;
    private static String TAG = "AdapterListProduk";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final CardView LLProduk;
        public final TextView tvnamaproduk;
        public final TextView tvhargaproduk;
        public final ImageView imgproduk;
        public final ImageView imgedit;
        public final ImageView imghapus;
        public final TextView tvstatus;


        public MyViewHolder(View view) {
            super(view);
            LLProduk = view.findViewById(R.id.cv_Home);
            tvnamaproduk = view.findViewById(R.id.tvnamaproduk);
            tvhargaproduk = view.findViewById(R.id.tvhargaproduk);
            imgproduk = view.findViewById(R.id.img_produk);
            imgedit = view.findViewById(R.id.imgedit);
            imghapus = view.findViewById(R.id.imghapus);
            tvstatus = view.findViewById(R.id.tvstatusprod);


        }
    }

    public adapter_list_produk(Context context, List<item_data> ListMenu) {
        this.mContext = context;
        this.ListMenu= ListMenu;
        userHelper_sqlite = new UserHelper_sqlite(mContext.getApplicationContext());
        pDialog = new ProgressDialog(mContext);
    }

    @Override
    public adapter_list_produk.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_list_produk, parent, false);

        return new adapter_list_produk.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final adapter_list_produk.MyViewHolder holder, int position) {
        item = ListMenu.get(position);

        holder.tvnamaproduk.setText(item.getNamaProduk());
        holder.tvhargaproduk.setText("Rp. "+item.getHargaProduk());
        Glide.with(mContext)
                .load(item.getUrlProduk())
                .error(R.drawable.kaos)
                .fitCenter()
                .into(holder.imgproduk);
        idproduk = item.getIdProduk();

        holder.LLProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                Intent edit = new Intent(mContext, EditFotoProdukActivity.class);
                edit.putExtra("idproduk", idproduk);
                edit.putExtra("nama_prd",item.getNamaProduk());
                edit.putExtra("desc_prd", item.getDescProduk());
                edit.putExtra("harga_prd", item.getHargaProduk());
                edit.putExtra("brand_prd", item.getBrandProduk());
                edit.putExtra("tag_prd", item.getTagProduk());
                edit.putExtra("url1", item.getURLProduk1());
                edit.putExtra("url2", item.getURLProduk2());
                edit.putExtra("url3", item.getURLProduk3());
                edit.putExtra("url4", item.getURLProduk4());

                edit.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(edit);
              //  ((Activity)mContext).finish();
            }
        });

        holder.imghapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                //ask
                new MaterialDialog.Builder(view.getContext())
                        .title(R.string.app_name)
                        .content("Yakin Ingin Menghapus Data Ini ?")
                        .positiveText("OK")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                SendHapus(idproduk);

                            }
                        })
                        .show();
            }
        });

        holder.imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent edit = new Intent(mContext, EditProdukActivity.class);
                edit.putExtra("idproduk", idproduk);
                edit.putExtra("nama_prd",item.getNamaProduk());
                edit.putExtra("desc_prd", item.getDescProduk());
                edit.putExtra("harga_prd", item.getHargaProduk());
                edit.putExtra("brand_prd", item.getBrandProduk());
                edit.putExtra("tag_prd", item.getTagProduk());
                edit.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(edit);

            }
        });

        if(item.getStatusSurat().equals("0") ){
            holder.tvstatus.setText("Terkirim");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }

        else if(item.getStatusSurat().equals("1")){

            holder.tvstatus.setText("Disetujui");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorGreen));

        }

        else if(item.getStatusSurat().equals("2")){

            holder.tvstatus.setText("Ditolak");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorRed));

        }

        else if(item.getStatusSurat().equals("9")){

            holder.tvstatus.setText("Dibaca");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorGreen));

        }


    }

    private void setpilih(){
      /* */

    }
    //

    private void SendHapus(final String stridproduk){
      //  showPDialog("Loading ..");
        // Creating volley request obj
        Log.d("IDPRODUK", stridproduk);
        StringRequest Req_register = new StringRequest(Request.Method.POST,
                AppConfig.URL_HapusProduk, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                //hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){
                        DialogPesan("Sukses", "Data Berhasil Terhapus");
                    }
                    else if(KodeRespon.equals("05")){
                        DialogPesan("Gagal", "Data Berhasil Terhapus");
                    }
                    else {
                        DialogPesan("Gagal", "Silakan ulangi");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idproduk", stridproduk);
                //params.put("role_type", "1");
                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_register);
    }



    @Override
    public int getItemCount() {
        return ListMenu.size();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    private void DialogPesan(final String judul, final String pesan){
        new MaterialDialog.Builder(mContext)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


                    }
                })
                .show();
    }

}




