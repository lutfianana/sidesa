package nzhi.apps.sidesa.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import nzhi.apps.sidesa.R;
import nzhi.apps.sidesa.activity.EditFixSuratActivity;
import nzhi.apps.sidesa.helper.UserHelper_sqlite;
import nzhi.apps.sidesa.model.item_data;

public class adapter_list_surat extends RecyclerView.Adapter<adapter_list_surat.MyViewHolder> {
    private List<item_data> ListMenu;
    private Context mContext;
    UserHelper_sqlite userHelper_sqlite;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final LinearLayout LLSurat;
        public final TextView tvnamasurat;
        public final TextView tvstatus;
        public final TextView tvtanggal;




        public MyViewHolder(View view) {
            super(view);
            LLSurat = view.findViewById(R.id.LLSurat);
            tvnamasurat = view.findViewById(R.id.tvnamaproduk);
            tvstatus = view.findViewById(R.id.tvstatus);
            tvtanggal = view.findViewById(R.id.tvtanggal);


        }
    }

    public adapter_list_surat(Context context, List<item_data> ListMenu) {
        mContext = context;
        this.ListMenu= ListMenu;
        userHelper_sqlite = new UserHelper_sqlite(mContext.getApplicationContext());
    }

    @Override
    public adapter_list_surat.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_list_surat, parent, false);

        return new adapter_list_surat.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_list_surat.MyViewHolder holder, int position) {
        final item_data item = ListMenu.get(position);

        holder.tvnamasurat.setText("Surat "+item.getKeperluanSurat());
        holder.tvtanggal.setText(item.getTglSurat());




        holder.LLSurat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //cek
                if(!item.getStatusSurat().equals("1")){
                    //ke halaman edit
                    Intent edit = new Intent(mContext, EditFixSuratActivity.class);
                    edit.putExtra("stridsurat", item.getIdSurat());
                    edit.putExtra("strjenis",item.getIdJenis());
                    edit.putExtra("strtanggal", item.getTglSurat());
                    edit.putExtra("strnama", userHelper_sqlite.getobject("nama"));
                    edit.putExtra("strkeperluan", item.getKeperluanSurat());
                    edit.putExtra("strketerangan", item.getKetSurat());
                    edit.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   // edit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(edit);

                }
                else{
                    //alert


                }

            }
        });


        if(item.getStatusSurat().equals("0") ){
            holder.tvstatus.setText("Terkirim");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }

        else if(item.getStatusSurat().equals("1")){

            holder.tvstatus.setText("Disetujui");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorGreen));

        }

        else if(item.getStatusSurat().equals("2")){

            holder.tvstatus.setText("Ditolak");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorRed));

        }

        else if(item.getStatusSurat().equals("9")){

            holder.tvstatus.setText("Dibaca");
            holder.tvstatus.setTextColor(mContext.getResources().getColor(R.color.colorGreen));

        }




        //  holder.


        //holder.tvnama.setText(item.getNamaMenu());



    }


    @Override
    public int getItemCount() {
        return ListMenu.size();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}



